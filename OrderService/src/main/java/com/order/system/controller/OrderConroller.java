package com.order.system.controller;

import com.order.system.dto.Transaction;
import com.order.system.dto.UserHistoryDto;
import com.order.system.entity.Order;
import com.order.system.request.UserFundTransferRequest;
import com.order.system.service.OrderInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderConroller {


    @Autowired
    OrderInterface orderInterface;

    @RequestMapping(value = "/req", method = RequestMethod.POST)
    Transaction orderRequest(@RequestBody UserFundTransferRequest userFundTransferRequest) {
        return orderInterface.placeOrder(userFundTransferRequest);
    }
        @PostMapping("/history")
        List<Order> orderHistory(@RequestParam int pageNumber, @RequestParam int pageSize, @RequestParam String  userName){
        return orderInterface.orderHistory(pageNumber,pageSize,userName);
    }

}
