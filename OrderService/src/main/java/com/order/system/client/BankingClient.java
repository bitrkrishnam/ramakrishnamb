package com.order.system.client;

import com.order.system.dto.Transaction;
import com.order.system.dto.TransferBalanceRequest;
import com.order.system.request.UserFundTransferRequest;
import com.order.system.response.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="http://PAYMENT-BANK/account")
public interface BankingClient {

 @RequestMapping(value = "/sendmoney", method = RequestMethod.POST)
 public Transaction sendMoney(@RequestBody TransferBalanceRequest transferBalanceRequest);

}


