package com.order.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data

public class Transaction  {

    private Long transactionId;
    private Long fromAccountNumber;
    private Long toAccountNumber;
    private BigDecimal transactionAmount;
    private String transactionDateTime;
}
