package com.order.system.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserHistoryDto {
    private Long orderId;
    private Long userId;
    private String userName;
    private  String itemCode;
    private BigDecimal price;
    private String date;

}
