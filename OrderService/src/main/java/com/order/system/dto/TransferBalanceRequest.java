package com.order.system.dto;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class TransferBalanceRequest {
    private Long fromAccountNumber;
    private Long toAccountNumber;
    private int amount;


}