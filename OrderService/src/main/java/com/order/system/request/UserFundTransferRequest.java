package com.order.system.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserFundTransferRequest {
    private  long userAccountNumber;
    private String name;
    private int orderedPrice;
    private  String itemCode;
}
