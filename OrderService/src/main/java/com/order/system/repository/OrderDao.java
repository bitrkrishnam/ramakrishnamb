package com.order.system.repository;

import com.order.system.dto.UserHistoryDto;
import com.order.system.entity.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderDao extends JpaRepository<Order,Long> {
   List<Order> findByUserNameEquals(String userName, Pageable pageable);
}
