package com.order.system.service;

import com.order.system.dto.Transaction;
import com.order.system.dto.UserHistoryDto;
import com.order.system.entity.Order;
import com.order.system.request.UserFundTransferRequest;

import java.util.List;

public interface OrderInterface {

    Transaction placeOrder(UserFundTransferRequest order);
    List<Order> orderHistory(int pageNumber, int pageSize, String userName);
}
