package com.order.system.service.impl;

import com.order.system.client.BankingClient;
import com.order.system.dto.Transaction;
import com.order.system.dto.TransferBalanceRequest;
import com.order.system.dto.UserHistoryDto;
import com.order.system.entity.Order;
import com.order.system.repository.OrderDao;
import com.order.system.request.UserFundTransferRequest;
import com.order.system.service.OrderInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class OrderInterfaceImpl implements OrderInterface {

    @Autowired
    OrderDao orderDao;
    @Autowired
    BankingClient bankingClient;

    private AtomicLong value = new AtomicLong(623456234523L);

    @Override
    public Transaction placeOrder(UserFundTransferRequest userFundTransferRequest) {
        Order newOrder=new Order();
        DateTimeFormatter pattern = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm");
        newOrder.setItemCode(userFundTransferRequest.getItemCode());
        newOrder.setPrice(BigDecimal.valueOf(userFundTransferRequest.getOrderedPrice()));
        newOrder.setUserName(userFundTransferRequest.getName());
        LocalDateTime localDateTime=LocalDateTime.now();
        newOrder.setUserId(value.getAndIncrement());
        newOrder.setDate(localDateTime.format(pattern));
        orderDao.save(newOrder);
        TransferBalanceRequest transferBalanceRequest=new TransferBalanceRequest();
        transferBalanceRequest.setFromAccountNumber(userFundTransferRequest.getUserAccountNumber());
        transferBalanceRequest.setAmount(userFundTransferRequest.getOrderedPrice());
        transferBalanceRequest.setToAccountNumber(6232895506281l);
        return bankingClient.sendMoney(transferBalanceRequest);
    }

    @Override
    public List<Order> orderHistory(int pageNumber,int pageSize,String userName) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Sort.Direction.DESC, "date"));
        return orderDao.findByUserNameEquals(userName,pageable);

    }
}
