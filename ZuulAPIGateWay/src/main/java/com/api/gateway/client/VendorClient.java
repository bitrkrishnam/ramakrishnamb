package com.api.gateway.client;

import com.api.gateway.model.VendorMenu;
import com.api.gateway.response.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name="http://VENDOR-SERVICE/vendors")
public interface VendorClient {

    @RequestMapping(value = "/findfoodtype", method = RequestMethod.GET)
    public ResponseEntity<List<VendorMenu>> findFoodItem(@RequestParam("foodType") String foodType);

    @RequestMapping(value = "/loadfooditems", method = RequestMethod.POST)
    public Response loadFoodMenu();

}
