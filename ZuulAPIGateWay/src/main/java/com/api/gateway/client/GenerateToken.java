package com.api.gateway.client;

import com.api.gateway.dto.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="http://USER-SERVICE/token")
public interface GenerateToken {

    @RequestMapping(value = "/generate", method = RequestMethod.POST)
     User findUsername(@RequestParam("üsername") String username);
}
