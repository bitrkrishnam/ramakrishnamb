package com.api.gateway.client;

import com.api.gateway.dto.UserDto;
import com.api.gateway.dto.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="http://USER-SERVICE/registration")
public interface UserSignupClient {

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public User signup(@RequestBody UserDto user);

}
