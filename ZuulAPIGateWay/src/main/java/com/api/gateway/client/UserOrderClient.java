package com.api.gateway.client;

import com.api.gateway.dto.Order;
import com.api.gateway.dto.Transaction;
import com.api.gateway.dto.UserHistoryDto;
import com.api.gateway.dto.UserOrderRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="http://ORDER-SERVICE/orders")
public interface UserOrderClient {

    @RequestMapping(value = "/req", method = RequestMethod.POST)
    Transaction orderRequest(@RequestBody UserOrderRequest userOrderRequest);

    @PostMapping("/history")
    List<Order> orderHistory(@RequestParam("pageNumber")  int pageNumber, @RequestParam("pageSize") int pageSize, @RequestParam("userName") String  userName);
}
