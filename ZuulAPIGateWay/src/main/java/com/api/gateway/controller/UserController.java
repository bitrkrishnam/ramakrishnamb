package com.api.gateway.controller;

import com.api.gateway.client.UserOrderClient;
import com.api.gateway.client.VendorClient;
import com.api.gateway.dto.Order;
import com.api.gateway.dto.Transaction;
import com.api.gateway.dto.UserHistoryDto;
import com.api.gateway.dto.UserOrderRequest;
import com.api.gateway.model.VendorMenu;
import com.api.gateway.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/user")
public class UserController {

    @Autowired
    VendorClient vendorClient;

    @Autowired
    UserOrderClient orderClient;

    @RequestMapping(value = "/findfoodtype", method = RequestMethod.GET)
    public ResponseEntity<List<VendorMenu>> findFoodItem(@RequestParam String foodType){
        return vendorClient.findFoodItem(foodType);
    }

    @PostMapping(value="/loadfooditems")
    public Response loadFoodMenu(){
        return Response.ok().setPayload(vendorClient.loadFoodMenu());
    }

    @RequestMapping(value = "/req", method = RequestMethod.POST)
    ResponseEntity<Transaction> orderRequest(@RequestBody UserOrderRequest userOrderRequest){
        return  ResponseEntity.ok(orderClient.orderRequest(userOrderRequest));

    }
    @RequestMapping(value = "/history", method = RequestMethod.GET)
    ResponseEntity<List<Order>> orderHistory(@RequestParam int pageNumber, @RequestParam int pageSize, @RequestParam String  userName){
        return ResponseEntity.ok(orderClient.orderHistory(pageNumber,pageSize,userName));

    }
}
