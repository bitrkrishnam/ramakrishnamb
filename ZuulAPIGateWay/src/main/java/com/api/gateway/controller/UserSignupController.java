package com.api.gateway.controller;

import com.api.gateway.dto.UserDto;
import com.api.gateway.dto.User;
import com.api.gateway.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/signup")
public class UserSignupController {

    @Autowired
    UserService userService;
    @RequestMapping(value = "/newuser", method = RequestMethod.POST)
    public User register(@RequestBody UserDto user){
        return userService.save(user);
    }
}
