package com.api.gateway.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class VendorMenu {

    private Integer id;
    private String itemCode;
    private String  foodItem;
    private BigDecimal price;
}