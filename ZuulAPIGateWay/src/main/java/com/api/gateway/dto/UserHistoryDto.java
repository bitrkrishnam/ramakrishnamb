package com.api.gateway.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class UserHistoryDto {
    private Long orderId;
    private Long userId;
    private String userName;
    private  String itemCode;
    private BigDecimal price;
    private LocalDateTime date;

}
