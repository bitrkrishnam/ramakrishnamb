package com.api.gateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Order implements Serializable {

    private Long orderId;
    private Long userId;
    private String userName;
    private String itemCode;
    private BigDecimal price;
    private String date;
}
