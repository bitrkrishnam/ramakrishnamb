package com.api.gateway.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class User {
    @JsonIgnore
    private int id;
    private String username;
    private String password;

}
