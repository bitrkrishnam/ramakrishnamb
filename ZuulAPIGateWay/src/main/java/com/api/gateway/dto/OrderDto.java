package com.api.gateway.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class OrderDto {
    private Integer id;
    private long userId;
    private  String item_code;
    private BigDecimal ordered_price;
    private LocalDateTime ordered_date;
}
