package com.api.gateway.dto;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class UserOrderRequest {
    private  long userAccountNumber;
    private String name;
    private int orderedPrice;
    private  String itemCode;

}
