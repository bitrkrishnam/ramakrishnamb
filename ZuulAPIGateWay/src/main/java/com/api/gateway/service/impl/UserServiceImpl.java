package com.api.gateway.service.impl;


import com.api.gateway.client.GenerateToken;
import com.api.gateway.client.UserSignupClient;
import com.api.gateway.dto.UserDto;
import com.api.gateway.dto.User;
import com.api.gateway.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {


	@Autowired
	UserSignupClient userSignupClient;

	@Autowired
	GenerateToken generateToken;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;
	private AtomicLong value = new AtomicLong(101456234523L);

	@Override
	public User save(UserDto user) {
		user.setPassword(bcryptEncoder.encode(user.getPassword()));
		User newUser = userSignupClient.signup(user);
		return newUser;
	}

	@Override
	public User findOne(String username) {
		return generateToken.findUsername(username);
	}


	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User newUser = generateToken.findUsername(username);
		if (newUser == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(newUser.getUsername(), newUser.getPassword(), getAuthority());
	}


	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}
}





