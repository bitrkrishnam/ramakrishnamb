package com.api.gateway.service;


import com.api.gateway.dto.UserDto;
import com.api.gateway.dto.User;

public interface UserService {

    User save(UserDto user);
    User findOne(String username);

}
