package com.devglan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devglan.model.User;
import com.devglan.model.UserDto;
import com.devglan.service.UserService;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

	@Autowired
	private UserService userService;

		@RequestMapping(value = "/new", method = RequestMethod.POST)
		public User signup(@RequestBody UserDto user) {
		return 	userService.save(user);
	}

}
