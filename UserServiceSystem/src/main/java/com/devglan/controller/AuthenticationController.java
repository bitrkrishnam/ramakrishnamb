package com.devglan.controller;


import com.devglan.model.*;
import com.devglan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/token")
public class AuthenticationController {


    @Autowired
    private UserService userService;

    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public User findUsername(@RequestParam("üsername") String username){

       return userService.findOne(username);

    }

}
