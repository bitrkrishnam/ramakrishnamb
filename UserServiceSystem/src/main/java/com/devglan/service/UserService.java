package com.devglan.service;

import com.devglan.model.*;
import java.util.List;

public interface UserService {

    User save(UserDto user);
    User findOne(String username);

}
