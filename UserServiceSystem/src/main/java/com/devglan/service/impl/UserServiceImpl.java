package com.devglan.service.impl;

import com.devglan.dao.UserDao;
import com.devglan.model.*;
import com.devglan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;


@Service
public class UserServiceImpl implements  UserService {

	@Autowired
	private UserDao userDao;

	private AtomicLong value = new AtomicLong(101456234523L);

	@Override
	public User save(UserDto user) {
		User newUser = new User();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(user.getPassword());
		return userDao.save(newUser);
	}

	@Override
	public User findOne(String username) {
		return userDao.findByUsername(username);
	}


}



