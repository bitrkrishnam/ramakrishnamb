package com.vendor.system.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="vendor")
public class Vendor {
    @Id
    @Column(name = "vendor_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="item_code")
    private String itemCode;

    @Column(name="food_item")
    private String  foodItem;

    @Column(name="price")
    private BigDecimal price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItemCode() {
        return itemCode;
    }

    public Vendor setItemCode(String itemCode) {
        this.itemCode = itemCode;
        return this;
    }

    public String getFoodItem() {
        return foodItem;
    }

    public Vendor setFoodItem(String foodItem) {
        this.foodItem = foodItem;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Vendor setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

}
