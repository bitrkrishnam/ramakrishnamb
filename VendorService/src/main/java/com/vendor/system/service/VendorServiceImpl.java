package com.vendor.system.service;

import com.vendor.system.entity.Vendor;
import com.vendor.system.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VendorServiceImpl implements VendorService {
    @Autowired
    VendorRepository vendorRepository;

    String line="";

    @Override
    public void loadFoodMenu() {

          try{
              BufferedReader br= Files.newBufferedReader(Paths.get("C:\\FoodApplication\\VendorService\\src\\main\\resources\\Food-menu.csv"));
              List<String> list=  br.lines().skip(1).collect(Collectors.toList());
                 list.stream().map(temp->{
                     Vendor vendor=new Vendor();
                     String[] data = temp.split(",");
                     vendor.setItemCode(data[0]).setFoodItem(data[1]).setPrice(BigDecimal.valueOf(new Double(data[2])));
                        vendorRepository.save(vendor);
                        return  vendor;

                }).collect(Collectors.toList());

          } catch (IOException e) {
              e.printStackTrace();
          }

      }

   @Override
    public List<Vendor> findFoodType(String foodType) {

        return  vendorRepository.findByFoodItemLike(foodType);
    }

}
