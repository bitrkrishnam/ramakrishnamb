package com.vendor.system.service;

import com.vendor.system.entity.Vendor;

import java.util.List;


public interface VendorService {
     void loadFoodMenu();
     List<Vendor> findFoodType(String foodType);
}
