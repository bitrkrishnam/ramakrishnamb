package com.vendor.system.controller;

import com.vendor.system.entity.Vendor;
import com.vendor.system.response.Response;
import com.vendor.system.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vendors")
public class VendorController {

    @Autowired
    VendorService vendorService;

    @PostMapping(value="/loadfooditems")
    public Response loadFoodMenu(){
        vendorService.loadFoodMenu();
        return Response.ok().setPayload("Food menu data loaded from excel to Data base");
    }

    @RequestMapping(value = "/findfoodtype", method = RequestMethod.GET)
    public List<Vendor> findFoodType(@RequestParam String foodType){
           return vendorService.findFoodType(foodType);
    }


}
