package com.vendor.system.repository;

import com.vendor.system.entity.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VendorRepository extends JpaRepository<Vendor,Integer> {
    @Query(value="select * from vendor v where v.food_item like %:foodItem%",nativeQuery = true)
    List<Vendor> findByFoodItemLike(@Param("foodItem")String foodItem);

    Vendor findByitemCode(String itemCode);

}
